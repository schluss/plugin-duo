/* DUO Plugin 
 * source: https://gitlab.com/schluss/plugin-duo

 */

let config = {
	
	duourl : 'https://services.schluss.app/duo',		// the providers' login website
	downloadmatch : 'blob:'								// the (part of) the filename to match in url to catch the download
};

module.exports = {
	// init is called when the plugin fires, context contains all kinds of handy information about the request and this provider
    init:  (context) => {
		
		// open inappbrowser 
		let ref = window.open(config.duourl, '_blank', 'location=no,hidden=yes,zoom=no,beforeload=yes');
	
		ref.addEventListener('beforeload', async (event, callback) => { 
			console.log("InappBrowser beforeload triggered, url: " + event.url);
			
			if (event.url.search(config.downloadmatch) != -1){
	
				console.log('match found, url contains: ' + config.downloadmatch);
				
				// hide inappbrowser
				ref.close();
				
				// show processing screen
				context.processingView();				
				
				let json = await download(event.url);
								
				// make the payload package in the right format to share it to the store function 
				
				/* required format (JSON string):
 				
				{
					"payload" : [
						{"name" : "aaa", "value" : "bbb"},
						{"name" : "xxx", "value" : "yyy"}
					]
				}

				*/
				
				let result = {payload : []};
				result.payload.push({ 
					name : context.request.provider.fields[0].match, 
					value : json.studydebt});
				
				// store the payload in the app
				context.store(JSON.stringify(result), context.request.id).then(() =>{
					
					// show processing done screen
					context.processingCompleted();
				
				});
			}
			else {
				callback(event.url);
			}
		});
		
		ref.addEventListener('loadstart', (event) => { 
			console.log("InappBrowser loadstart triggered, url: " + event.url);
			ref.show();				
		});	

		ref.addEventListener('exit', (event) => {
			console.log("InappBrowser exit event triggered");
			
			ref.close();
			ref = undefined;
		});		
		
	}
	
};

async function download(url){
	let result = await fetch(url);
	let json = await result.json();
	return json;
}